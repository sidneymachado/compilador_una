var app = angular.module('StarterApp', ['ngMaterial', 'ui.ace']);

app.controller('AppCtrl', ['$scope', '$mdToast', '$mdBottomSheet', '$animate', '$timeout', '$mdSidenav', '$mdUtil', '$log', 'Constantes', 'Servicos', function($scope, $mdToast, $mdBottomSheet, $animate, $timeout, $mdSidenav, $mdUtil, $log, Constantes, Servicos){

    $scope.codigo = "algoritmo(){\n\t" +
    "limpatela();\n\t" +
    "real x;\n\t" +
    "inteiro y;\n\t" +
    "string mensagem;\n\t" +
    "real z;\n\t" +
    "leia(x);\n\t" +
    "leia(y);\n\t" +
    "se(x > y){\n\t\t" +
    "escreva(x);\n\t" +
    "}\n\t" +
    "z = x + y;\n\t" +
    "mensagem = \"O resultado é\";\n\t" +
    "imprima(mensagem);\n\t" +
    "escreva(z);\n" +
    "}";

    var diagram = null;

    $scope.getBlocos = function(token, proximoIndex){
        var blocoEncontrado = null;
        if(proximoIndex === undefined){
            proximoIndex = $scope.codigo.indexOf(token);
            blocoEncontrado = $scope.codigo.substr(0, proximoIndex);
        }
        else{
            var tokenIndex = $scope.codigo.indexOf(token, proximoIndex + 1);
            if(tokenIndex === -1){
                if(token === '}'){
                    return
                }
                token = '}';
            }
            else{
                blocoEncontrado = $scope.codigo.substr(proximoIndex + 1, tokenIndex - proximoIndex);
                proximoIndex = tokenIndex;
            }
        }
        if(blocoEncontrado != '' && blocoEncontrado != null){
            $scope.blocos.push(blocoEncontrado);
        }
        $scope.getBlocos(token, proximoIndex);

    };

    $scope.processOutput = function(){
        $scope.blocos = [];
        $scope.mapaTipos = {};
        $scope.mapaValores = {};

        $scope.executaProximo = true;

        $scope.validarSintaxe($scope.codigo);

        if($scope.codigo){

            $scope.getBlocos('{');
            $scope.execucaoCodigo();
            $scope.execucaoFluxograma();

        }
    };

    $scope.getFatores = function(fatorA, fatorB){
        var valorA;
        var valorB;
        var validaFatorA = $scope.validarTipoVariavel(fatorA, $scope.mapaValores[fatorA]);
        if(!validaFatorA[0]){
            throw validaFatorA[1];
        }
        else{
            valorA = validaFatorA[1];
        }

        var validaFatorB = $scope.validarTipoVariavel(fatorB, $scope.mapaValores[fatorB]);
        if(!validaFatorB[0]){
            throw validaFatorB[1];
        }
        else{
            valorB = validaFatorB[1];
        }

        return [valorA, valorB];
    };

    $scope.imprimir = function(valor){

        var valorSaida = $scope.validarTipoVariavel(valor);
        if(!valorSaida[0]){
            $scope.saida = valorSaida[1];
        }

        else{
            var saidaAtual = $scope.saida;
            if(saidaAtual != undefined){
                $scope.saida = saidaAtual + '\n' + valorSaida[1];
            }
            else{
                $scope.saida = valorSaida[1];
            }
        }

    };

    $scope.validarTipoVariavel = function(variavel, valor){
        var tipo = $scope.mapaTipos[variavel];
        var valorRet = null;

        if(tipo === undefined){

            if(!isNaN(variavel)){
                return [true, parseFloat(variavel)];
            }

            else if(typeof variavel === "string"){
                return [true, new String(variavel).toString()];
            }

            return [false, "A variável "+variavel+" não foi informada"];
        }

        if(tipo === "inteiro" && valor != undefined){
            if(isNaN(valor)){
                return [false, variavel + " deve ser um número inteiro"];
            }
            valorRet = parseInt(valor);
        }

        else if(tipo === "real" && valor != undefined){
            if(isNaN(valor)){
                return [false, variavel + " deve ser um número real"];
            }
            valorRet = parseFloat(valor);
        }

        else if(tipo === "string"){
            var valorRet = $scope.mapaValores[variavel];
            if(valorRet){
                if(valorRet.indexOf('"') === -1){
                    return [false, "'"+variavel + "' deve ser uma string"];
                }
                valorRet = valorRet.split('"')[1];
            }
        }

        return [true, valorRet];
    };

    $scope.toastPosition = {
        bottom: true,
        top: false,
        left: true,
        right: false
    };
    $scope.getToastPosition = function() {
        return Object.keys($scope.toastPosition)
            .filter(function(pos) { return $scope.toastPosition[pos]; })
            .join(' ');
    };

    $scope.exibirAlerta = function(mensagem){
        $mdToast.show(
            $mdToast.simple()
                .content(mensagem)
                .position($scope.getToastPosition())
                .hideDelay(10000)
        );
    };

    $scope.$watch('codigo', function(newValue, oldValue){
        var novoCodigo = newValue.split("\n");
        var velhoCodigo = oldValue.split("\n");
        if(novoCodigo.length != velhoCodigo.length){
            $scope.validarSintaxe(newValue);
        }
    });

    $scope.validarSintaxe = function(value){

        var code = value.split("\n");

        for (var i = 0; i < code.length; i++){
            var bloco = code[i].trim();
            if(bloco != ''
                    && bloco.indexOf('{') == -1
                    && bloco.indexOf('}') == -1
                    && bloco.indexOf(';') == -1 ){
                $scope.exibirAlerta('Parece haver um erro na linha ' + (i+1) );
                break;
            }
        }
    };

    $scope.toggleRight = buildToggler('right');

    function buildToggler(navID) {
        var debounceFn =  $mdUtil.debounce(function(){
            $mdSidenav(navID)
                .toggle()
                .then(function () {
                    $log.debug("toggle " + navID + " is done");
                });
        },300);
        return debounceFn;
    }

    $scope.isCondicaoValida = function(condicao){

        if(condicao.indexOf("==") > -1){
            var igual1 = condicao.split("==")[0].trim();
            var igual2 = condicao.split("==")[1].trim();
            var fatoresIgual = $scope.getFatores(igual1, igual2);
            if(fatoresIgual[0] == fatoresIgual[1]){
                return true;
            }
        }

        else if(condicao.indexOf("!=") > -1){
            var diferente1 = condicao.split("!=")[0].trim();
            var diferente2 = condicao.split("!=")[1].trim();
            var fatoresDiferente = $scope.getFatores(diferente1, diferente2);
            if(fatoresDiferente[0] != fatoresDiferente[1]){
                return true;
            }
        }

        else if(condicao.indexOf(">=") > -1){
            var maiorIgual1 = condicao.split(">=")[0].trim();
            var maiorIgual2 = condicao.split(">=")[1].trim();
            var fatoresMaiorIgual = $scope.getFatores(maiorIgual1, maiorIgual2);
            if(fatoresMaiorIgual[0] >= fatoresMaiorIgual[1]){
                return true;
            }
        }

        else if(condicao.indexOf("<=") > -1){
            var menorIgual1 = condicao.split("<=")[0].trim();
            var menorIgual2 = condicao.split("<=")[1].trim();
            var fatoresMenorIgual = $scope.getFatores(menorIgual1, menorIgual2);
            if(fatoresMenorIgual[0] <= fatoresMenorIgual[1]){
                return true;
            }
        }

        else if(condicao.indexOf(">") > -1){
            var maior1 = condicao.split(">")[0].trim();
            var maior2 = condicao.split(">")[1].trim();
            var fatoresMaior = $scope.getFatores(maior1, maior2);
            if(fatoresMaior[0] > fatoresMaior[1]){
                return true;
            }
        }

        else if(condicao.indexOf("<") > -1){
            var menor1 = condicao.split("<")[0].trim();
            var menor2 = condicao.split("<")[1].trim();
            var fatoresMenor = $scope.getFatores(menor1, menor2);
            if(fatoresMenor[0] < fatoresMenor[1]){
                return true;
            }
        }

        return false;

    };

    $scope.addOperacaoFluxograma = function(j, i, bloco){
        $scope.addFluxograma(j, i , bloco, 'op', 'operation');

    };

    $scope.addEntradaFluxograma = function(j, i, bloco){
        $scope.addFluxograma(j, i , bloco, 'io', 'inputoutput');
    };

    $scope.addCondicionalFluxograma = function(j, i, bloco){
        $scope.addFluxograma(j, i , bloco, 'cond', 'condition');
    };

    $scope.addFluxograma = function(j, i, bloco, alias, tipo){
        var _alias = alias+''+j+''+i;
        $scope.codFluxograma += _alias+'=>'+tipo+': ' + bloco + '\n';

        if('condition' === tipo){
            $scope.condicaoAtual = _alias;
            $scope.seqFluxosCondicionais[_alias+'-yes'] = '\n'+_alias+'(yes)->';
            $scope.seqFluxosCondicionais[_alias+'-no'] = '\n'+_alias+'(no)->';
            $scope.seqFluxograma += _alias;
        }
        else if($scope.condicaoAtual !== null){
            if($scope.dentroCondicao){
                $scope.seqFluxosCondicionais[$scope.condicaoAtual+'-yes'] += _alias+'->';
            }
            else if($scope.dentroSenao){
                $scope.seqFluxosCondicionais[$scope.condicaoAtual+'-no'] += _alias+'->';
            }
            else{
                $scope.seqFluxosCondicionais[$scope.condicaoAtual+'-yes'] += _alias+'->';
                $scope.seqFluxosCondicionais[$scope.condicaoAtual+'-no'] += _alias+'->';
            }
        }
        else{
            $scope.seqFluxograma += _alias + '->';
        }

        return _alias;
    };

    $scope.execucaoCodigo = function(){
        var executaSenao = true;
        var dentroLaco = false;
        var condicaoLaco = null;

        for(var j=1; j < $scope.blocos.length; j++){
            var _bloco = $scope.blocos[j];
            if(!$scope.executaProximo){
                $scope.executaProximo = true;
                continue;
            }

            if(dentroLaco){
                if(!$scope.isCondicaoValida(condicaoLaco)){
                    dentroLaco = false;
                    continue;
                }
                else{
                    j -= 1;
                }
            }

            var code = _bloco.split(";");

            for (var i = 0; i < code.length; i++) {
                var bloco = code[i].trim();
                var regPar = /\(([^)]+)\)/g;

                if(Servicos.isInteiro(bloco)){
                    var name = bloco.split(Constantes.inteiro)[1].trim();
                    $scope.mapaTipos[name] = Constantes.inteiro;
                }

                else if(Servicos.isReal(bloco)){
                    var nomeReal = bloco.split(Constantes.real)[1].trim();
                    $scope.mapaTipos[nomeReal] = Constantes.real;
                }

                else if(Servicos.isString(bloco)){
                    var nomeString = bloco.split(Constantes.string)[1].trim();
                    $scope.mapaTipos[nomeString] = Constantes.string;
                }

                else if(bloco.indexOf("senao") > -1){
                    $scope.executaProximo = executaSenao;
                }

                else if(bloco.indexOf("se") > -1){
                    var matchSe = regPar.exec(bloco);
                    var condicao = matchSe[1];
                    $scope.executaProximo = $scope.isCondicaoValida(condicao);
                    executaSenao = !$scope.executaProximo;
                }

                else if (bloco.indexOf('enquanto') > -1){
                    var matchEnquanto = regPar.exec(bloco);
                    condicaoLaco = matchEnquanto[1];
                    if($scope.isCondicaoValida(condicaoLaco)){
                        dentroLaco = true;
                    }
                    else{
                        $scope.executaProximo = false;
                    }
                }

                else if(bloco.indexOf("=") > -1){
                    var variavel = bloco.split("=")[0].trim();
                    var valor = bloco.split("=")[1].trim();

                    if(valor.indexOf("+") > -1){
                        var somaNum1 = valor.split("+")[0].trim();
                        var somaNum2 = valor.split("+")[1].trim();
                        var fatoresSoma = $scope.getFatores(somaNum1, somaNum2);
                        var resultadoSoma = fatoresSoma[0] + fatoresSoma[1];
                        var valSoma = $scope.validarTipoVariavel(variavel, resultadoSoma);
                        if(!valSoma[0]){
                            $scope.saida = valSoma[1];
                            break;
                        }
                        else{
                            $scope.mapaValores[variavel] = valSoma[1];
                        }
                        continue;
                    }

                    if(valor.indexOf("-") > -1){
                        var subNum1 = valor.split("-")[0].trim();
                        var subNum2 = valor.split("-")[1].trim();
                        var fatoresSub = $scope.getFatores(subNum1, subNum2);
                        var resultadoSub = fatoresSub[0] - fatoresSub[1];
                        var valSub = $scope.validarTipoVariavel(variavel, resultadoSub);
                        if(!valSub[0]){
                            $scope.saida = valSub[1];
                            break;
                        }
                        else{
                            $scope.mapaValores[variavel] = valSub[1];
                        }
                        continue;
                    }

                    if(valor.indexOf("*") > -1){
                        var multNum1 = valor.split("*")[0].trim();
                        var multNum2 = valor.split("*")[1].trim();
                        var fatoresMult = $scope.getFatores(multNum1, multNum2);
                        var resultadoMult = fatoresMult[0] * fatoresMult[1];
                        var valMult = $scope.validarTipoVariavel(variavel, resultadoMult);
                        if(!valMult[0]){
                            $scope.saida = valMult[1];
                            break;
                        }
                        else{
                            $scope.mapaValores[variavel] = valMult[1];
                        }
                        continue;
                    }

                    if(valor.indexOf("/") > -1){
                        var divNum1 = valor.split("/")[0].trim();
                        var divNum2 = valor.split("/")[1].trim();
                        var fatoresDiv = $scope.getFatores(divNum1, divNum2);
                        var resultadoDiv = fatoresDiv[0] / fatoresDiv[1];
                        var valDiv = $scope.validarTipoVariavel(variavel, resultadoDiv);
                        if(!valDiv[0]){
                            $scope.saida = valDiv[1];
                            break;
                        }
                        else{
                            $scope.mapaValores[variavel] = valDiv[1];
                        }
                        continue;
                    }

                    var validacao = $scope.validarTipoVariavel(variavel, valor);
                    if(!validacao[0]){
                        $scope.saida = validacao[1];
                        break;
                    }
                    else{
                        $scope.mapaValores[variavel] = valor;
                    }

                }

                else if(bloco.indexOf("leia") > -1){
                    var m = regPar.exec(bloco);
                    var variavel = m[1];

                    var validacao = $scope.validarTipoVariavel(variavel);
                    if(!validacao[0]){
                        $scope.saida = validacao[1];
                        break;
                    }

                    var valor = window.prompt("Valor de " + variavel);
                    validacao = $scope.validarTipoVariavel(variavel, valor);
                    if(!validacao[0]){
                        $scope.saida = validacao[1];
                        break;
                    }
                    else{
                        $scope.mapaValores[variavel] = valor;
                    }
                }

                else if(bloco.indexOf("escreva")>-1){
                    var m = regPar.exec(bloco);
                    var variavel = m[1];
                    var validacao = $scope.validarTipoVariavel(variavel);
                    if(!validacao[0]){
                        $scope.saida = validacao[1];
                        break;
                    }

                    var valorSaida = $scope.mapaValores[variavel];
                    $scope.imprimir(valorSaida);

                }

                else if(bloco.indexOf("limpatela") > -1){
                    $scope.saida = undefined;
                }

                else if(Servicos.isImprima(bloco)){
                    var m = regPar.exec(bloco);
                    var variavel = m[1];
                    $scope.imprimir(variavel);

                }

            }

        }
    }

    $scope.execucaoFluxograma = function(){
        $scope.dentroCondicao = false;
        $scope.dentroSenao = false;
        var fimCondicao = 0;
        var fimSenao = 0;
        $scope.seqFluxosCondicionais = new Map();
        $scope.condicaoAtual = null;

        $scope.codFluxograma = 'st=>start: Início\n';
        $scope.seqFluxograma = 'st->';

        for(var j=1; j < $scope.blocos.length; j++){
            var _bloco = $scope.blocos[j];

            if(fimCondicao == j){
                $scope.dentroCondicao = false;
            }

            if(fimSenao == j){
                $scope.dentroSenao = false;
            }

            var code = _bloco.split(";");

            for (var i = 0; i < code.length; i++) {
                var bloco = code[i].trim();
                var regPar = /\(([^)]+)\)/g;

                if(bloco.indexOf("senao") > -1){
                    $scope.dentroSenao = true;
                    fimSenao = j + 2;
                }

                else if(bloco.indexOf("se") > -1 || bloco.indexOf('enquanto') > -1){
                    var matchSe = regPar.exec(bloco);
                    var condicao = matchSe[1];
                    $scope.dentroCondicao = true;
                    fimCondicao = j + 2;
                    $scope.addCondicionalFluxograma(j, i, condicao);
                }

                else if(bloco.indexOf("=") > -1){
                    $scope.addOperacaoFluxograma(j, i, bloco);
                }

                else if(bloco.indexOf("leia") > -1 || bloco.indexOf("escreva")>-1 || bloco.indexOf("imprima") > -1){
                    $scope.addEntradaFluxograma(j, i, bloco);
                }

            }

        }

        $scope.codFluxograma += 'e=>end: Fim\n';

        for(var seq in $scope.seqFluxosCondicionais){
            $scope.seqFluxograma += $scope.seqFluxosCondicionais[seq];
            $scope.seqFluxograma += 'e';
        }

        $scope.codFluxograma += $scope.seqFluxograma;
        if(diagram){
            diagram.clean();
        }
        console.log($scope.codFluxograma);
        diagram = flowchart.parse($scope.codFluxograma);
        diagram.drawSVG('diagram');
    }

}]);

app.controller('OpcoesCtrl', function ($scope, $timeout, $mdSidenav, $log){

    $scope.funcoesDisponiveis = [
        'leia(variavel)', 'escreva(variavel)', 'limpatela()', 'imprima()', 'se(condição)', 'enquanto(condição)'
    ];

    $scope.tiposDisponiveis = [
        'inteiro', 'real'
    ];

    $scope.operacoesDisponiveis = [
        '+', '-', '*', '/'
    ];

    $scope.close = function () {
        $mdSidenav('right').close()
            .then(function () {
                $log.debug("close RIGHT is done");
            });
    };

    $scope.adicionarCodigo = function(saida){
        $scope.codigo = saida;
        $log.debug(saida);
    }
});
