app.service("Servicos", function(Constantes){

    this.getOperacao = function(bloco){
        if(bloco.indexOf(Constantes.inteiro)>-1){
            return Constantes.inteiro;
        }
        else if(bloco.indexOf(Constantes.real)>-1){
            return Constantes.real;
        }
        else if(bloco.indexOf(Constantes.string)>-1){
            return Constantes.string;
        }
        else if(bloco.indexOf(Constantes.imprima)>-1){
            return Constantes.imprima;
        }
        return null;
    };

    this.isInteiro = function(bloco){
        return this.getOperacao(bloco) == Constantes.inteiro;
    };

    this.isReal = function(bloco){
        return this.getOperacao(bloco) == Constantes.real;
    };

    this.isString = function(bloco){
        return this.getOperacao(bloco) == Constantes.string;
    };

    this.isImprima = function(bloco){
        return this.getOperacao(bloco) == Constantes.imprima;
    };

});

app.service("Fluxograma", function(){
    this.add = function(){
        return null;
    };
});
